#requires -version 2
<#
.SYNOPSIS
  Script to reorganize music files into 'year - month' coded folders based on either the creation date or the last modified date

.DESCRIPTION
  <Brief description of script>

.PARAMETER <Parameter_Name>
    <Brief description of parameter input required. Repeat this attribute if required>

.INPUTS
  A root directory which contains the music files to reorganize

.OUTPUTS
  A copy of all the music files in the root directory in new 'year - month' coded folders

.NOTES
  Version:        1.1
  Author:         Jasper Timmermans
  Creation Date:  06-13-2017
  Updated: 12-10-2017

.EXAMPLE
  <Example goes here. Repeat this attribute for more than one example>
#>

Param (
    [switch] $Recurse
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

[int]$i = 0
[int]$Copied = 0
[int]$Skipped = 0
$recurse = $true
$FolderSelector = New-Object -ComObject Shell.Application
$SourceFolder = ($FolderSelector.BrowseForFolder(0, 'Select the Source folder', 0)).Self.Path
$DestinationRootFolder = ($FolderSelector.BrowseForFolder(0, 'Select the Destination folder', 0)).Self.Path

#-----------------------------------------------------------[Execution]------------------------------------------------------------
#Get creationtime and lastwritetime for Tracks
$TrackCollection = Get-ChildItem -LiteralPath $SourceFolder -File -Recurse:$Recurse
$TrackCollection | Select-Object name, CreationTime, LastWriteTime | Format-Table -AutoSize

Write-Host "Number of Tracks: $($TrackCollection.count)" -ForegroundColor Yellow
$SortBy = Read-Host "Organize Tracks by CreationTime (1) or LastWriteTime (2)?"

foreach ($Track in $TrackCollection) {
    $CreationYear = $Track.CreationTime.Year
    $CreationMonth = $Track.CreationTime.Month
    $LastWriteYear = $Track.LastWriteTime.Year
    $LastWriteMonth = $Track.LastWriteTime.Month
    $SourceFile = $Track.FullName

    switch ($SortBy) {
        '1' { $DestinationFolder = "$($DestinationRootFolder)\$($CreationYear) - $($CreationMonth)" }
        '2' { $DestinationFolder = "$($DestinationRootFolder)\$($LastWriteYear) - $($LastWriteMonth)" }
    }
    if (!(Test-Path $DestinationFolder)) {
        New-Item -ItemType "directory" -Path $DestinationFolder -ErrorAction SilentlyContinue
    }
    $Destination = "$($DestinationFolder)\$($Track.name)"

    #start copy job
    Write-Progress -Activity 'Copying Tracks...' -PercentComplete ($i / $TrackCollection.count * 100) -CurrentOperation $Track.name
    if (!(Test-Path $Destination)) {
        Copy-Item -LiteralPath $SourceFile -Destination "$DestinationFolder" -Verbose
        $Copied ++
    }
    else {
        Write-Warning "$($Track.name) already exists at target location, skipping..."
        $Skipped ++
    }
    $i++
}
Write-Output "Script complete: `n
Copied: $($Copied) files `n
Skipped: $($Skipped) files"